package com.example.kate.guessnumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.*;

public class MainActivity extends AppCompatActivity {
    TextView tvInfo;
    TextView tvError;
    EditText etInput;
    Button bControl;
    TableRow trError;
    int guess;
    boolean gameFinished;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvInfo = (TextView)findViewById(R.id.textView);
        tvError = (TextView)findViewById(R.id.textErrorView);
        etInput = (EditText)findViewById(R.id.editText);
        bControl = (Button)findViewById(R.id.button);
        trError = (TableRow)findViewById(R.id.tableRowError);

        guess = (int)(Math.random()*100);
        gameFinished = false;
    }
    public void onClick(View v){
        if (!gameFinished){
            try {
                int inp = Integer.parseInt(etInput.getText().toString());
                if(inp > 100 || inp < 0) {
                    tvError.setText(getResources().getString(R.string.error3));
                    trError.setVisibility(View.VISIBLE);
                    return;
                }
                if (inp > guess)
                    tvInfo.setText(getResources().getString(R.string.ahead));
                else if (inp < guess)
                    tvInfo.setText(getResources().getString(R.string.behind));
                else if (inp == guess) {
                    tvInfo.setText(getResources().getString(R.string.hit));
                    bControl.setText(getResources().getString(R.string.play_more));
                    gameFinished = true;
                }
                if(trError.getVisibility() == View.VISIBLE)
                    trError.setVisibility(View.GONE);
                float newOpacity = 1 - Math.abs(inp - guess)/100f;
                etInput.setAlpha(newOpacity);
                return;
            }
            catch (Exception ex)
            {
                tvError.setText(getResources().getString(R.string.error2));
                trError.setVisibility(View.VISIBLE);
            }
        }
        else
        {
            guess = (int)(Math.random()*100);
            bControl.setText(getResources().getString(R.string.input_value));
            tvInfo.setText(getResources().getString(R.string.try_to_guess));
            gameFinished = false;
        }
        etInput.setText("");
    }
}
